# 新型コロナウィルス 世界の感謝状況アプリ

## javscriptプロジェクトにtypescript適用する
0. JSDocで試す
1. typescript basic環境
   - [x] NPM初期化
   - [x] typescriptライブラリ設置
   - [x] typescript設定ファイル作成
   - [x] javscriptファイルをtypescriptファイルへ変換
   - [x] `tsc` コマンドでtypescriptコンパイル
2. `any`指定
   - `tsconfig.json`ファイルに`noImplicitAny`値を`true`で設定
   - 可能な限り具体的なtype指定
3. プロジェクト環境設定
   - babel, eslint, prettierなど
4. 外部ライブラリーのモジュール化
5. `strict`オプション追加後のtype指定


## 参考資料

- [ジョンズ・ホプキンス大学の新型コロナウイルス感染状況](https://www.arcgis.com/apps/opsdashboard/index.html#/bda7594740fd40299423467b48e9ecf6)
- [Postman API](https://documenter.getpostman.com/view/10808728/SzS8rjbc?version=latest#27454960-ea1c-4b91-a0b6-0468bb4e6712)
- [Type Vue without Typescript](https://blog.usejournal.com/type-vue-without-typescript-b2b49210f0b)